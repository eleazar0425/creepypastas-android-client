package estrella.eleazar.com.creepypastas;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import estrella.eleazar.creepypastas.localdata.Base64ImageDecoder;

/**
 * Created by Eliazar on 13/12/2015.
 */
public class Articulo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.articulo);

        TextView titulo,texto;
        ImageView image;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarArticulo);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //titulo = (TextView) findViewById(R.id.tituloArticulo);
        texto = (TextView) findViewById(R.id.textoArticulo);
        image = (ImageView) findViewById(R.id.image_paralax);


        Creepy creepy = (Creepy) getIntent().getSerializableExtra("creepy");

        if(creepy.getImage() == null)
            image.setImageResource(R.drawable.default_background);
        else
            image.setImageBitmap(Base64ImageDecoder.decodeByteArray(creepy.getImage()));

        //titulo.setText(creepy.getTitulo());
        texto.setText(creepy.getTexto());
        getSupportActionBar().setTitle(creepy.getTitulo());
    }
}
