package estrella.eleazar.com.creepypastas;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by Eliazar on 13/12/2015.
 */
public class Creepy implements Serializable {

    private String titulo, categoria, texto, idcreepypasta, base64image;
    private byte[] image;

    public Creepy(String idcreepypasta, String titulo, String categoria, String texto, String base64image){
        this.idcreepypasta = idcreepypasta;
        this.titulo = titulo;
        this.categoria = categoria;
        this.texto = texto;
        this.base64image = base64image;
    }

    public Creepy(String idcreepypasta, String titulo, String categoria, String texto){
        this.idcreepypasta = idcreepypasta;
        this.titulo = titulo;
        this.categoria = categoria;
        this.texto = texto;
    }

    public Creepy(String idcreepypasta, String titulo, String categoria, String texto, byte[] image){
        this.idcreepypasta = idcreepypasta;
        this.titulo = titulo;
        this.categoria = categoria;
        this.texto = texto;
        this.image = image;
    }
/*
    public void setTitulo(String titulo){
        this.titulo = titulo;
    }

    public void setCategoria(String categoria){
        this.categoria = categoria;
    }

    public void setTexto(String texto){
        this.texto = texto;
    }*/

    public String getTitulo(){return titulo;}
    public String getCategoria(){return categoria;}
    public String getTexto(){return texto;}
    public String getIdcreepypasta(){return idcreepypasta;}
    public String getBase64image(){return base64image;}

    public String getMiniTexto(){
        String newText = texto.replaceAll("\n", " ");
        return newText.substring(0,137)+"...";
    }

    public String getMiniTitulo(){
        String newText;
        if(titulo.length()>20){
            newText = titulo.substring(0, 20);
            return newText + "...";
        }else{
            return titulo;
        }
    }

    public void destroyBase64Image(){
        base64image = null;
    }

    public void setImage(byte[] image){
        this.image = image;
    }

    public byte[] getImage(){
        return image;
    }
}
