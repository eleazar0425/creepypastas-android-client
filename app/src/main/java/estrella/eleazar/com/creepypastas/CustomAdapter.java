package estrella.eleazar.com.creepypastas;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Eliazar on 13/12/2015.
 */
public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.CreepyHolder> {

    private List<Creepy> items;

    public static class CreepyHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView titulo, categoria, texto;
        ItemClickListener listener;

        public CreepyHolder(View v, ItemClickListener listener){
            super(v);
            titulo = (TextView) v.findViewById(R.id.titulo);
            categoria = (TextView) v.findViewById(R.id.genero);
            texto = (TextView) v.findViewById(R.id.texto);
            this.listener = listener;

           v.setOnClickListener(this);
        }

        public void onClick(View view){
            listener.onItemClick(view, getAdapterPosition());
        }

        public static interface ItemClickListener{
            void onItemClick(View view, int position);
        }
    }

    public CustomAdapter(List<Creepy> items){
        this.items = items;
    }

    @Override
    public CreepyHolder onCreateViewHolder(ViewGroup group, int i){
        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.cardview, group, false);
        return new CreepyHolder(view, new CreepyHolder.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(view.getContext(), Articulo.class);
                intent.putExtra("creepy", items.get(position));
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public void onBindViewHolder(CreepyHolder holder, int i){
        holder.titulo.setText(items.get(i).getMiniTitulo());
        holder.categoria.setText(items.get(i).getCategoria());
        holder.texto.setText(items.get(i).getMiniTexto());
    }

    @Override
    public int getItemCount(){
        return items.size();
    }
}
