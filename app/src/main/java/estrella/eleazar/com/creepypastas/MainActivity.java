package estrella.eleazar.com.creepypastas;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.miguelcatalan.materialsearchview.MaterialSearchView;
import java.util.ArrayList;
import java.util.List;
import estrella.eleazar.creepypastas.localdata.DBA;
import estrella.eleazar.serverdata.DataParser;
import estrella.eleazar.sicronizacion.TaskSincronizacion;

/*
    @TODO
    Agregar eventos de NavigationView y MaterialSearchView
    Compatibilidad con imagenes
    Caching imagenes descargadas desde el servidor
    Logica de las busquedas
    Activity para favoritos
 */
public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<Creepy> items = new ArrayList();
    private CustomAdapter adapter;
    private TaskSincronizacion task;
    private DataParser dataParser;
    private DrawerLayout drawerLayout;
    private NavigationView nav;
    private MaterialSearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setToolbar();

        searchView = (MaterialSearchView) findViewById(R.id.search_view);
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {

            }

            @Override
            public void onSearchViewClosed() {

            }
        });

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        nav = (NavigationView) findViewById(R.id.nav_view);
        nav.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        drawerLayout.closeDrawers();
                        switch(menuItem.getItemId()){
                            case R.id.nav_acerca_de:
                                Intent intent = new Intent(MainActivity.this, AcercaDe.class);
                                startActivity(intent);
                                menuItem.setChecked(true);
                                return true;
                        }
                        return true;
                    }
                }
        );

        LinearLayoutManager manager = new LinearLayoutManager(this);

        adapter = new CustomAdapter(items);

        recyclerView = (RecyclerView) findViewById(R.id.mainView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);

        dataParser = new DataParser(this, items, adapter);

        task = new TaskSincronizacion(this, items, adapter);
        task.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
           case R.id.refresh:
                dataParser.getDataFromServer(DBA.getLastId());
                return true;
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed(){
        if(searchView.isSearchOpen()){
            searchView.closeSearch();
        }else{
            super.onBackPressed();
        }
    }

    private void setToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);
    }
}