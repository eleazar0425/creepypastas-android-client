package estrella.eleazar.creepypastas.localdata;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

/**
 * Created by Eliazar on 08/02/2016.
 */
public class Base64ImageDecoder {

    public static byte[] decode(String base64String){
        byte[] decodedByte = Base64.decode(base64String,0);
        return decodedByte;
    }

    public static Bitmap decodeByteArray(byte[] byteArray){
        if(byteArray == null){
            return null;
        }else{
            return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        }
    }

    public static byte[] encode(Bitmap bitmap){
       if(bitmap == null){
           return null;
       }else{
           ByteArrayOutputStream baos = new ByteArrayOutputStream();
           bitmap.compress(Bitmap.CompressFormat.PNG, 100,baos);
           byte[] b = baos.toByteArray();
           return b;
       }
    }
}
