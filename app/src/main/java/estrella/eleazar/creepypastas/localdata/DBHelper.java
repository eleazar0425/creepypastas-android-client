package estrella.eleazar.creepypastas.localdata;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Eliazar on 13/12/2015.
 */
class DBHelper extends SQLiteOpenHelper {

    private static final String DBNAME = "creepydb";
    private static final int DBVERSION = 1;

    public DBHelper(Context context){
        super(context, DBNAME, null, DBVERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL(QUOTES.CREATE_TABLE_CATEGORIA);
        db.execSQL(QUOTES.INITIAl_INSERT);
        db.execSQL(QUOTES.CREATE_TABLE_CREEPYPASTA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){}
}
