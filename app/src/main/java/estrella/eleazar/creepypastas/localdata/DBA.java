package estrella.eleazar.creepypastas.localdata;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.List;

import estrella.eleazar.com.creepypastas.Creepy;

/**
 * Created by Eliazar on 13/12/2015.
 */
public class DBA {

    /*
    Clase encargada de crear la base de datos y gestionar la entra y salida de datos localmente

     */

    private static DBHelper helper;
    private static  SQLiteDatabase db;
    private static Cursor c;

    public DBA(Context context){
        helper = new DBHelper(context); //crea la base de datos
        db = helper.getWritableDatabase(); //Obtener permisos de lectura y escritura
    }

    public List<Creepy> getAllCreepys(){
        List<Creepy> items = new ArrayList<>();
        c = db.rawQuery(QUOTES.GET_ALL_SCRIPT, null);

        while(c.moveToNext()){
            items.add(new Creepy(c.getString(0), c.getString(1), c.getString(2), c.getString(3),
                    c.getBlob(4)));
        }
        return items;
    }

    public void insertCreepy(List<Creepy> creepy){
        for(int i=0; i<creepy.size(); i++){
            int categoria = 0;
            switch (creepy.get(i).getCategoria()){
                case "Historias":
                    categoria = QUOTES.CATEGORIAS.HISTORIAS;
                    break;
                case "Internet":
                    categoria = QUOTES.CATEGORIAS.INTERNET;
                    break;
                case "Series de TV":
                    categoria = QUOTES.CATEGORIAS.SERIES_TV;
                    break;
                case "Videojuegos":
                    categoria = QUOTES.CATEGORIAS.VIDEOJUEGOS;
                    break;
                case "Caricaturas":
                    categoria = QUOTES.CATEGORIAS.CARICATURAS;
                    break;
                case "Canciones":
                    categoria = QUOTES.CATEGORIAS.CANCIONES;
                    break;
            }
            ContentValues values = new ContentValues();
            if(categoria!=0){ //Si la categoria sigue siendo igual a 0 es porque hay un error ajeno a esta clase
                values.put("idcreepypasta", Integer.parseInt(creepy.get(i).getIdcreepypasta()));//Debido a
                values.put("titulo", creepy.get(i).getTitulo());//problemas de parseo provinientes del lado del
                values.put("texto", creepy.get(i).getTexto());//servidor, es necesario expresar el id como
                values.put("idcategoria", categoria);//un entero, y hacer las conversiones del lado del cliente
                values.put("base64image", creepy.get(i).getImage());
                db.insert("creepypasta", null, values);
            }
        }
    }

    public static int getLastId(){
        c = db.rawQuery(QUOTES.GET_LAST_ARTICULO, null);
        if(c.moveToFirst()){
            return c.getInt(0);
        }else{
            return 0;
        }
    }
}
