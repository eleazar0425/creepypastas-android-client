package estrella.eleazar.creepypastas.localdata;

import android.app.Activity;
import android.content.Context;
import java.util.List;
import estrella.eleazar.com.creepypastas.Creepy;
import estrella.eleazar.com.creepypastas.CustomAdapter;

/**
 * Created by Eliazar on 08/02/2016.
 */
public class InsertingLocalThread extends Thread {
    private List<Creepy> items, adapterList;
    private DBA dba;
    private CustomAdapter adapter;
    private Activity activity;

    public InsertingLocalThread(List<Creepy> items, Activity activity, List<Creepy> adapterList, CustomAdapter adapter){
        this.items = items;
        this.dba = new DBA(activity.getApplicationContext());
        this.adapterList = adapterList;
        this.adapter = adapter;
        this.activity = activity;
    }

    @Override
    public void run() {
        for(int i=0; i<items.size(); i++){
            if(items.get(i).getBase64image() != null) {
                items.get(i).setImage(Base64ImageDecoder.decode(items.get(i).getBase64image()));
                items.get(i).destroyBase64Image();
            }
        }
        dba.insertCreepy(items);
        adapterList.clear();
        adapterList.addAll(dba.getAllCreepys());

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }
}
