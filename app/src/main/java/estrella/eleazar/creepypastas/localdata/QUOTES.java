package estrella.eleazar.creepypastas.localdata;

/**
 * Created by Eliazar on 13/12/2015.
 */
public class QUOTES {

    public static class CATEGORIAS{
        public static final int HISTORIAS = 1;
        public static final int INTERNET = 2;
        public static final int SERIES_TV = 3;
        public static final int VIDEOJUEGOS = 4;
        public static final int CARICATURAS = 5;
        public static final int CANCIONES = 6;
    }

    public static final String CREATE_TABLE_CATEGORIA = "CREATE TABLE CATEGORIA (" +
            "IDCATEGORIA INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
            "NOMBRE VARCHAR(150) NOT NULL" +
            ")";

    public static final String CREATE_TABLE_CREEPYPASTA = "CREATE TABLE CREEPYPASTA(" +
            "IDCREEPYPASTA INTEGER PRIMARY KEY NOT NULL," +
            "TITULO VARCHAR(500) NOT NULL," +
            "TEXTO TEXT NOT NULL," +
            "BASE64IMAGE BLOB,"+
            "IDCATEGORIA INT NOT NULL," +
            "FOREIGN KEY (IDCATEGORIA) REFERENCES CATEGORIA(IDCATEGORIA)" +
            ")";

    public static final String INITIAl_INSERT = "INSERT INTO CATEGORIA VALUES(" +
            "NULL,'Historias')," +
            "(NULL, 'Internet')," +
            "(NULL, 'Series de TV')," +
            "(NULL, 'Videojuegos')," +
            "(NULL, 'Caricaturas')," +
            "(NULL, 'Canciones')";

    public static final String GET_ALL_SCRIPT = "SELECT CREEPYPASTA.IDCREEPYPASTA, CREEPYPASTA.TITULO, " +
            "CATEGORIA.NOMBRE, CREEPYPASTA.TEXTO, CREEPYPASTA.BASE64IMAGE " +
            "FROM CREEPYPASTA INNER JOIN CATEGORIA ON CREEPYPASTA.IDCATEGORIA = CATEGORIA.IDCATEGORIA";

   public static final String GET_LAST_ARTICULO = "SELECT IDCREEPYPASTA FROM CREEPYPASTA ORDER BY IDCREEPYPASTA" +
           " DESC LIMIT 1";
}
