package estrella.eleazar.sicronizacion;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;
import java.util.List;
import estrella.eleazar.com.creepypastas.Creepy;
import estrella.eleazar.com.creepypastas.CustomAdapter;
import estrella.eleazar.creepypastas.localdata.DBA;
import estrella.eleazar.serverdata.DataParser;

public class TaskSincronizacion extends AsyncTask<Void, Integer, Void> {
        private int lastId = 0;
        private DBA dba;
        private DataParser parser;
        private List<Creepy> items;
        private CustomAdapter adapter;
        private Activity activity;
        private ProgressDialog dialog;

        public TaskSincronizacion(Activity activity, List<Creepy> items, CustomAdapter adapter){
            dba = new DBA(activity.getApplicationContext());
            parser = new DataParser(activity, items, adapter);
            dialog = new ProgressDialog(activity);
            dialog.setCancelable(false);
            this.items = items;
            this.adapter = adapter;
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            lastId = dba.getLastId();
            if(lastId!=0){
                items.addAll(dba.getAllCreepys());
                adapter.notifyDataSetChanged();
            }
            if(getStatus()==Status.PENDING){
                dialog.setMessage("Verificando si hay nuevos articulos...");
                dialog.show();
            }else{
                if(dialog.isShowing()){dialog.dismiss();}
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            dialog.setMessage("Obteniendo nuevos articulos");
            if(lastId==0){
                parser.getDataFromServer(lastId);
                if(dialog.isShowing()){dialog.dismiss();}
            }else{
                try{
                    if(dialog.isShowing()){dialog.dismiss();}
                    int i = 1000 * 60 * 60 * 8; //Dormir hilo 8 horas
                    Thread.sleep(i);
                    parser.getDataFromServer(lastId);
                    lastId = dba.getLastId();
                }catch (InterruptedException e){
                    e.printStackTrace();
                };
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(dba.getAllCreepys().size()>0){
                items.clear();
                items.addAll(dba.getAllCreepys());
                adapter.notifyDataSetChanged();
                Toast.makeText(activity.getApplicationContext(), "DB"+dba.getAllCreepys().size(), Toast.LENGTH_LONG).show();
                Toast.makeText(activity.getApplicationContext(), "lastid" + lastId, Toast.LENGTH_LONG).show();
            }
            if(dialog.isShowing()){dialog.dismiss();}
        }
    }