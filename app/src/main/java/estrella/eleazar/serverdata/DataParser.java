package estrella.eleazar.serverdata;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Arrays;
import java.util.List;
import estrella.eleazar.com.creepypastas.Creepy;
import estrella.eleazar.com.creepypastas.CustomAdapter;
import estrella.eleazar.creepypastas.localdata.DBA;
import estrella.eleazar.creepypastas.localdata.InsertingLocalThread;

/**
 * Created by Eliazar on 13/12/2015.
 */
public class DataParser {

    private List<Creepy> items;
    private CustomAdapter adapter;
    private Gson gson;
    private Activity context;
    private DBA dba;

    public DataParser(Activity context, List<Creepy> items, CustomAdapter adapter){
        gson = new Gson();
        this.items = items;
        this.context = context;
        dba = new DBA(context.getApplicationContext());
        this.adapter = adapter;
    }

    public void getDataFromServer(int idUltimoArticulo){
        String URL = MYSQLDATA.GETALLDATA+"?lastid="+idUltimoArticulo;
        VolleySingleton.getInstance(context.getApplicationContext()).addToRequestQueue(
                new JsonObjectRequest(
                        URL,
                        null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                procesar(response);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(context, "Error del servidor ex002 "+error.getMessage()+DBA.getLastId(), Toast.LENGTH_LONG).show();
                            }
                        })
        );
    }

    private void procesar(JSONObject object){
        List<Creepy> items;
        try {
            String estado = object.getString("estado");
            Toast.makeText(context, "estado: "+estado, Toast.LENGTH_LONG).show();
            switch (estado){
                case "1":
                    JSONArray datos = object.getJSONArray("datos");
                    Creepy[] list = gson.fromJson(datos.toString(), Creepy[].class);
                    items = Arrays.asList(list);
                    Toast.makeText(context, "tam: "+items.size(), Toast.LENGTH_LONG).show();
                    new InsertingLocalThread(items,context, this.items, adapter).start();
                    break;
                case "2":
                    Toast.makeText(context, "Se supone que esto nunca deba pasar...", Toast.LENGTH_SHORT).show();
                    break;
                case "3":
                    String message = object.getString("mensaje");
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    break;
            }
        }catch (JSONException e){
            Toast.makeText(context, "Error del servidor: ex01", Toast.LENGTH_SHORT).show();
        }
    }
}
