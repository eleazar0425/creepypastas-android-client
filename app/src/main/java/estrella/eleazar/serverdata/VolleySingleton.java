package estrella.eleazar.serverdata;
import android.content.Context;
import com.android.volley.RequestQueue;
import com.android.volley.Request;
import com.android.volley.toolbox.Volley;

/**
 * Created by Eliazar on 13/12/2015.
 */
public final class VolleySingleton {
    private static VolleySingleton volleySingleton;
    private RequestQueue request;
    private static Context context;


    private VolleySingleton(Context context){
        VolleySingleton.context = context;
        request = getRequestQueue();
    }

    public static synchronized VolleySingleton getInstance(Context context){
        if(volleySingleton==null){
            volleySingleton = new VolleySingleton(context.getApplicationContext());
        }
        return volleySingleton;
    }

    public RequestQueue getRequestQueue(){
        if(request==null){
            request = Volley.newRequestQueue(context.getApplicationContext());
        }
        return request;
    }

    public <T> void addToRequestQueue(Request<T> req){
        getRequestQueue().add(req);
    }
}